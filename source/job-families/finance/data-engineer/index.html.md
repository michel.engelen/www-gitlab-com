---
layout: job_family_page
title: "Data Engineer"
---

## Responsibilities

* Maintain our data warehouse with timely and quality data
* Build and maintain data pipelines from internal databases and SaaS applications
* Create and maintain architecture and systems documentation 
* Implement the [DataOps](https://en.wikipedia.org/wiki/DataOps) philosophy in everything you do
* Plan and execute system expansion as needed to support the company's growth and analytic needs
* Collaborate with Data Analysts to drive efficiencies for their work
* Collaborate with other functions to ensure data needs are addressed
* This position reports to the Manager, Data


## Requirements

* 2+ years hands-on experience in a data analytics or data warehousing role
* Demonstrably deep understanding of SQL and analytical data warehouses (Snowflake preferred)
* Experience with a high growth company using on-premise tools and on-demand (SaaS) transactional systems
* Hands-on experience with Python, SQL, and ETL tools like Talend/Kettle, Boomi, Informatica Cloud, etc. Experience with Snowflake is a plus
* Hands-on experience with data pipeline tools within cloud-based environments (Airflow, Luigi, Azkaban, dbt)
* Be Passionate about data, analytics, and automation.
* Experience with open source data warehouse tools
* Strong data modeling skills and familiarity with the Kimball methodology.
* Experience with Salesforce, Zuora, Zendesk and Marketo as data sources and consuming data from SaaS application APIs.
* Share and work in accordance with our values
* Constantly improve product quality, security, and performance
* Write good code, performant code (Python preferred)
* Knowledge of and experience with data-related Python packages
* Desire to continually keep up with advancements in data engineering practices
* Catch bugs and style issues in code reviews
* Ship small features independently

- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).


## Senior Data Engineer

The Senior Data Engineer role extends the [Data Engineer](#requirements) role.

### Responsibilities

* All requirements of an Intermediate Data Engineer
* Understand and implement data engineering best practices
* Write _great_ code
* Create smaller merge requests and issues by collaborating with stakeholders to reduce scope and focus on iteration
* Teach and enforce architectural patterns in code reviews
* Ship medium to large features independently
* Generate architecture recommendations and the ability to implement them
* Great communication: Regularly achieve consensus amongst teams
* Perform technical interviews

## Staff Data Engineer

The Staff Data Engineer role extends the [Senior Data Engineer](#senior-data-engineer) role.

### Responsibilities
* Advocate for improvements to data quality, security, and performance that have particular impact across your team and others.
* Solve technical problems of the highest scope and complexity for your team.
* Exert significant influence on the overall objectives and long-range goals of your team.
* Shepherd the definition and improvement of our internal standards for style, maintainability, and best practices for a high-scale reporting environment. Maintain and advocate for these standards through code review.
* Drive innovation on the team with a willingness to experiment and to boldly confront problems of immense complexity and scope.
* Actively seek out difficult impediments to our efficiency as a team ("technical debt"), propose and implement solutions that will enable the entire team to iterate faster
* Represent GitLab and its values in public communication around broad initiatives, specific projects, and community contributions. Interact with customers and other external stakeholders as a consultant and spokesperson for the work of your team. 
* Provide mentorship for all Analysts and Engineers on your team to help them grow in their technical responsibilities and remove blockers to their autonomy.
* Confidently ship large features, reports, and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.


## Specialties

### Architecture

* Focus on end-to-end systems and processes across the data stack
* Drive efficiency optimizations at the extract, load, transform, and reporting layers data infrastructure

### Analytics

* Proactively identify and address Data Analyst pain points and processes by creating better tooling and documentation
* Evangelize the data team's output to other functions 
* Drive efficiencies around the transform and reporting layers of the data infrastructure
* Oversee [coverage rotations](/handbook/business-ops/data-team/#dbt-test-coverage-rotation)
* Manage the Data Analyst Intern program

## PIs
*   [SLO achievement per data source](/handbook/business-ops/metrics/)
*   [Infrastructure Cost vs Plan](/handbook/business-ops/metrics/)
*   [Number of days since last enviroment audit](/handbook/business-ops/metrics/)
*   [Mean Time between Failures (MTBF)](/handbook/business-ops/metrics/)
*   [Mean Time to Repair (MTTR)](/handbook/business-ops/metrics/)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Manager, Data & Analytics
* Next, candidates will be invited to schedule a second interview with a Data Engineer on the Data & Analytics team
* Next, candidates will be invited to schedule a third interview with members of the Meltano team
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
