---
layout: markdown_page
title: "Category Vision - Usability Testing"
---

- TOC
{:toc}

## Usability Testing

GitLab has lots of capability for testing various aspects of software, but most are aimed around functional testing of code. Usability testing is a technique used in user-centered interaction design to evaluate a product by testing it on users. This can be seen as an irreplaceable usability practice, since it gives direct input on how real users use the system.

Usability testing is slightly different than UAT in that the primary purpose of a usability test is to improve a design, rather than a gate to accept/reject a deployment. At GitLab, we're not big believers in manual gates to slow things down, but we are big believers in user-focused design.

One thing that we certainly want to communicate through our vision here is that we feel designers are important, and can contribute using GitLab. By adding features with them in mind, we can help them to contribute their best.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AUsability%20Testing)
- [Overall Vision](https://about.gitlab.com/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1307) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Up next for Usability Testing is [gitlab-ce#62482](https://gitlab.com/gitlab-org/gitlab-cd/issues/62482), which will simplify the usage of the visual review tool. This will enable us to have a better sense of where to iterate on this tool next and bring an important capability to more users who are doing visual reviews.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

* [Improved UX for Visual Review Tools](https://gitlab.com/gitlab-org/gitlab-ee/issues/11322)
* [Add ability to take a screenshot in Visual Review Tool](https://gitlab.com/gitlab-org/gitlab-ee/issues/10765)
* [Add full-screen annotations to Visual Review tool](https://gitlab.com/gitlab-org/gitlab-ee/issues/10762)

## Competitive Landscape

Usability testing is a feature offered primarily through integrations by competitive products. Taking advantage of Review Apps, we're able to offer a much nicer experience via [gitlab-org&960](https://gitlab.com/groups/gitlab-org/-/epics/960) (manual review flow for review apps) where feedback can be requested on a review environment, bringing feedback much earlier into the development process and integrating that automatically with GitLab issues.

### Marker.io

[Marker.io](https://marker.io/) is a product that ships mostly as an extension to go into your browser and allows you to raise an issue with various issue providers (Trello, JIRA, GitHub and even GitLab).  It provides a very intuitive user interface for screenshots, annotations and other discussions.

## Top Customer Success/Sales Issue(s)

There are no top CS/Sales issues for this category.

## Top Customer Issue(s)

There are no top customer issues for this category.

## Top Internal Customer Issue(s)

For usability testing, our internal team has asked for built-in support for visual regression testing ([gitlab-ce#57111](https://gitlab.com/gitlab-org/gitlab-ce/issues/57111)).

## Top Vision Item(s)

For the same reasons as the competitive landscape section, [gitlab-org&960](https://gitlab.com/groups/gitlab-org/-/epics/960) (manual review flow for review apps) offers a very cool feature which allows design teams to collect user feedback on review apps and other environments. This is a strong foundation for how we can expand on usability testing within GitLab, without building UAT gates into the product.

Beyond this, we can look at offering the same for mobile via a solution like https://headspin.io/products.
