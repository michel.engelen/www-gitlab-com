---
layout: markdown_page
title: "Chief of Staff Team"
description: "GitLab CoS Team Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

{::options parse_block_html="true" /}

----


## Quick Links and Fun Facts
* [Chief of Staff Job Family](/job-families/chief-executive-officer/chief-of-staff/)
* [Internal Strategy Consultant Job Family](/job-families/chief-executive-officer/internal-strategy-consultant/)
* CoS = Chief of Staff
* CoST = Chief of Staff Team


## Contact Us
* [chief-of-staff-team](https://gitlab.slack.com/archives/CN7MPDZF0/p1568035351000200) on Slack

## Performance Indicators

<iframe class="dashboard-embed" src="https://app.periscopedata.com/shared/bcfef667-7d2b-45d7-9638-a23e196e2067?embed=true" height="1200"> </iframe>

### Throughput

Thoughput for the CoS team is measured as all MRs in [across GitLab Company namespaces](https://gitlab.com/gitlab-data/analytics/tree/master/transform/snowflake-dbt/macros#get-internal-parent-namespaces) divided by the number of team members.

### Executive Time for the CEO

Executive Time is measured through the CEO calendar. The [Sid's Calendar Export](https://docs.google.com/spreadsheets/d/1xVaH7zrY8MIwI2TbZrcQnc8yepuHFFk87Ga7KZTB28o/edit?usp=drive_web&ouid=113483692538021736976) is a private google sheet built of a google apps script. 
The script is triggered to download all of the Calendar data every time it is run. 
The `filtered_columns` tab on this sheet pulls only the relevant columns for analyses. 
This tab is then pulled by [SheetLoad](/handbook/business-ops/data-team/#using-sheetload) into the SheetLoad drive and then ingested into [Snowflake](/handbook/business-ops/data-team/#-data-warehouse). 
Events [are categorized based on the event name](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/models/sheetload/base/sheetload_calendar.sql#L20), as outlined by the [EBA team best practices](/handbook/eba/#eba-team-best-practices).
Those categories are applied to our [OKRs](/company/okrs/). 
Every event only gets one category. 
This is hard since some events may arguably fall into multiple. 
For example, a 1:1 with a CRO could go towards IACV, but since it's a 1:1 it goes towards Great Team, like all 1:1s.


## Daily Standup

On the CoST, we use [Geekbot](https://geekbot.com) for our Daily Standups.
These are posted in #chief-of-staff-team-standups in Slack.  
Once team members are added to the daily standup list, they will receive a message from Geekbot via DM once they've been active on Slack after 6 AM in their local timezone. 
There is no pressure to respond to Geekbot as soon as it messages you. 
When Geekbot asks, "What will you do today?" try answering with specific details.
Give responses to Geekbot that truly communicate to your team what you're working on that day, so that your team can help you understand if some priority has shifted or there is additional context you may need.